---
layout: layouts/base.njk
templateEngine: [md, njk]
---
# Обо мне

![Фото](static/img/photo.jpg){.avatar} Привет! Я Еленский Михаил и я DevOps Engineer из **Перми**.
Люблю Linux и Free и Open-Source Software и постоянно изучать новое. {.description}

## Навыки

- Языки: (уверенно) ![Логотип Python](https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg){.logo} Python,
  ![Логотип Bash](https://upload.wikimedia.org/wikipedia/commons/2/20/Bash_Logo_black_and_white_icon_only.svg){.logo} Shell-скриптинг (bash, fish),
  SQL;
  (основы)
  ![Логотип Rust](https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg){.logo} Rust,
  ![Логотип Golang](https://upload.wikimedia.org/wikipedia/commons/2/2d/Go_gopher_favicon.svg){.logo} Go,
  ![Логотип C/C++](https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg){.logo} C, C++,
  ![Логотип JS](https://upload.wikimedia.org/wikipedia/commons/9/99/Unofficial_JavaScript_logo_2.svg){.logo} JavaScript/TypeScript
- Технологии: (уверенно) ![Логотип Linux](https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg){.logo} GNU/Linux,
  ![Логотип Kubernetes](https://upload.wikimedia.org/wikipedia/commons/3/39/Kubernetes_logo_without_workmark.svg){.logo} Kubernetes,
  ![Логотип Docker](https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png){.logo} Docker [Swarm],
  ![Логотип Podman](static/img/podman-logo.svg){.logo} Podman,
  ![Логотип Helm](https://helm.sh/img/helm.svg){.logo} Helm
  ![Логотип ArgoCD](https://cncf-branding.netlify.app/img/projects/argo/icon/color/argo-icon-color.svg){.logo} Argo CD,
  ![Логотип git](https://git-scm.com/images/logos/downloads/Git-Icon-1788C.svg){.logo} git;
  (основы)
  ![Логотип Airflow](static/img/airflow-logo.svg){.logo} Apache Airflow,
  ![Логотип PostgreSQL](https://wiki.postgresql.org/images/a/a4/PostgreSQL_logo.3colors.svg){.logo} PostgreSQL,
  ![Логотип Airflow](static/img/mariadb-logo.svg){.logo} MariaDB [Galera],
  ![Логотип Redis](static/img/redis-logo.svg){.logo} Redis

🇬🇧 Английский на уровне Upper Intermediate


## Образование

- Бакалавр прикладной математики и информатики, [ПГНИУ](http://www.psu.ru/), 2022
- Программа повышения квалификации "Решение прикладных задач кибербезопасности с помощью Python", ПНИПУ, 2021

## Опыт работы

### DevOps Engineer @ [IT-Rev](https://itrev.ru)/[RevGames](https://rev.games)

Июль 2022 — настоящее время
- Перевёл dev и prod-инфраструктуру с выделенных VM на Kubernetes
- Провёл workshop по Kubernetes для команды
- Писал Helm-чарты для проектов
- Писал CI/CD-пайплайны (Gitlab CI)
- Внедрил методологию GitOps (ArgoCD)
- Поддерживал внутреннюю инфраструктуру команды, настраивал мониторинг
- Развёртывал и поддерживал СУБД (PostgreSQL, Mariadb, Clickhouse, Redis, Hazelcast)
- Участвовал в разработке и эксплуатации проектов геймификации, таких как ["Ловлю на слове (Сбер)"](https://slova.gamespasibo.ru),
    ["Зума Сити (Билайн)"](https://zumacity.beeline.ru/), ["Генератор подарков (Ростелеком)"](https://vk.com/wall-24720111_537407)
    и [других](https://rev.games)

### ML Engineer @ [LAB:CODE](https://lab-code.com)

Август 2020 — Январь 2022
- Работал над проектом распознавания речи (PyTorch, NVIDIA NeMo);
- Писал пайплайны для Apache Airfow;
- Провёл миграцию с Airflow 1 на Airflow 2;
- Настроил и запустил bare-metal Kubernetes-кластер для Airflow и проведения вычислительных экспериментов;
- Участвовал в преподавании основ Python и ML для студентов младших курсов;

# Учебные/pet проекты
- [News Events Miner](https://github.com/news-events-miner) ‒ распределённая система на базе Apache Spark и Kubernetes, предназначенная для извлечения событий из потоков текстов с помощью тематического моделирования.
- [Photo tagger](https://github.com/cringineers) ‒ распределённая система для автоматического аннотирования фотографий с помощью нейросети CLIP (командный проект, выполненный на хакатоне "Цифровой прорыв 2022")

# Контакты
📧 [Email](mailto:mkls-660@yandex.ru){.contact}
![Логотип Telegram](https://upload.wikimedia.org/wikipedia/commons/8/82/Telegram_logo.svg){.logo} [Telegram](https://t.me/mkls0){.contact}
![Логотип GitHub](https://upload.wikimedia.org/wikipedia/commons/9/91/Octicons-mark-github.svg){.logo} [GitHub](https://github.com/mkls6){.contact}
![Логотип Gitlab](https://about.gitlab.com/images/press/press-kit-icon.svg){.logo} [Gitlab](https://gitlab.com/mkls){.contact}

import lume from "lume/mod.ts";

const site = lume({src: './src'});
site.copy("favicons/favicon.ico", "favicon.ico");
site.copy('static');

export default site;
